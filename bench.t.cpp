#include <bheap.h>

#include <chrono>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <limits>
#include <queue>
#include <sstream>
#include <string>
#include <vector>

class TimeDiff {
  private:
    typedef std::chrono::high_resolution_clock d_clock;
    typedef std::chrono::duration<double, std::ratio<1>> d_second;
    std::chrono::time_point<d_clock> d_beg;

  public:
    TimeDiff() : d_beg(d_clock::now())
    {
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<d_second>(
                    d_clock::now() - d_beg).count();
    }
};

void printStats(const std::vector<double>& timings)
{
    double fastest = std::numeric_limits<double>::max();
    double sum = 0.0;
    for (auto i : timings) {
        fastest = std::min(fastest, i);
        sum += i;
    }
    const double avg = sum / static_cast<double>(timings.size() - 1);

    double sdvSum = 0.0;
    for (auto i : timings) {
        sdvSum += pow(i - avg, 2);
    }
    const double sdv = sqrt(sum / (timings.size() - 2));

    std::cout << std::fixed << std::setprecision(2)
              << fastest << '\t'
              << avg     << '\t'
              << sdv     << '\n';
}

template <typename Cont, typename VT>
void runTest(const std::string& name,
             const VT&          values,
             const VT&          otherValues)
{
    std::vector<double> timings;
    for (auto r = 0; r != 4; ++r) {
        TimeDiff diff;
        Cont pq;
        for (auto& i : values) {
            pq.push(i);
        }

        for (auto& i : otherValues) {
            pq.pop();
            pq.push(i);
        }

        for (auto& i : values) {
            (void) i;
            pq.pop();
        }
        timings.push_back(diff.elapsed());
    }
    std::cout << name << ' ' << values.size() << '\t';
    printStats(timings);
}

int main()
{
    std::cout << "\t\t\t\t\tFastest\tAverage\tStddev\n";

    std::string pqiTitle("std::priority_queue<int>>       ");
    std::string bhiTitle("BHeap<int>                      ");
    std::string pqsTitle("std::priority_queue<std::string>");
    std::string bhsTitle("BHeap<std::string>              ");
    std::string sep("----------------------------------------------------------------\n");

    // Load the large set of random values
    std::vector<int>         otherNumbers;
    std::vector<std::string> otherStrings;
    std::ostringstream ss;
    for (auto i = 0; i != 1000000; ++i) {
        ss.clear();
        ss.str("");
        auto rnd = rand() & 0x00003FFF;
        otherNumbers.push_back(rnd);
        ss << rnd;
        otherStrings.push_back(ss.str());
    }

    // Run the tests for each size
    for (auto n : {500, 1000, 5000, 10000}) {
        std::vector<int>         numbers;
        std::vector<std::string> strings;
        std::ostringstream ss;
        for (auto i = 0; i != n; ++i) {
            ss.clear();
            ss.str("");
            auto rnd = rand() & 0x00003FFF;
            numbers.push_back(rnd);
            ss << rnd;
            strings.push_back(ss.str());
        }


        runTest<std::priority_queue<int>>(pqiTitle,
                                          numbers,
                                          otherNumbers);
        runTest<BHeap<int>>(bhiTitle,
                            numbers,
                            otherNumbers);
        std::cout << sep;
        runTest<std::priority_queue<std::string>>(pqsTitle,
                                                  strings,
                                                  otherStrings);
        runTest<BHeap<std::string>>(bhsTitle,
                                    strings,
                                    otherStrings);
        std::cout << sep;
    }

    return 0;
}
