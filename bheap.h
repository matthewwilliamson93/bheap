#ifndef INCLUDED_BHEAP
#define INCLUDED_BHEAP

#include <cassert>
#include <cstdint>
#include <functional>
#include <iterator>
#include <utility>
#include <vector>

template <typename T, typename Compare = std::less<T>>
class BHeap {
  public:
    typedef std::vector<T>                           container_type;
    typedef typename container_type::value_type      value_type;
    typedef typename container_type::size_type       size_type;
    typedef typename container_type::reference       reference;
    typedef typename container_type::const_reference const_reference;

  private:
    static size_t getParentIndex(size_t u);
        // Returns parent index for the given child index.
        // Child index must be greater than 0.
        // Returns 0 if the parent is root.

    static size_t getChildIndex(size_t u);
        // Returns the index of the first child for the given parent index.
        // Parent index must be less than SIZE_MAX.
        // Returns SIZE_MAX if the index of the first child for the given parent
        // cannot fit size_t.

    void siftUp(size_t rootIndex, size_t holeIndex, const T& item);
        // Sifts the item up in the given sub-heap with the given root_index
        // starting from the hole_index.

    size_t moveUpMaxChild(size_t children_count, size_t hole_index, size_t child_index);
        // Moves the max child into the given hole and returns index
        // of the new hole.

    void siftDown(size_t holeIndex, const T& item);
        // Sifts the given item down in the heap of the given size starting
        // from the hole_index.

    bool isHeap() const;
        // Checks to see the internal vector follows the heap property.

    void makeHeap();
        // Takes an internal vector and makes it follow the heap property.

    void pushHeap();
        // Internal implementation of push.

    static const size_t PAGE_CHUNKS = 512;
    static const size_t PAGE_SIZE = PAGE_CHUNKS * 2;
    Compare        d_comp;
    container_type d_c;

  public:
    explicit BHeap(const Compare& comparer = Compare());
        // A constructor for a bheap that can optionally take a comparator.

    template <typename InputIterator>
    BHeap(const InputIterator& first,
          const InputIterator& last,
          const Compare&       comparer = Compare());
        // A constructor which can take two input iterators and an optional
        // comparator.

    bool empty() const;
        // See if any items are currently in the bheap.

    size_t size() const;
        // Return the size of all items in the bheap.

    size_t capacity() const;
        // Return the capacity of the internal vector in the bheap.

    const T& top() const;
        // Return the top item in the bheap.

    void reserve(size_t capacity);
        // Preallocate space in the internal vector in the bheap based on the
        // capacity provided.

    void push(const T& v);
        // Push an item onto the bheap.

    void push(T&& v);
        // Push an R value reference onto the bheap.

    void pop();
        // Pops the maximum item from the bheap.

    void swap(BHeap& bh);
        // Swap two bheaps.
};

template <typename T, typename Compare>
size_t BHeap<T, Compare>::getParentIndex(size_t index)
{
    assert(0 < index);

    --index;

    if (index < 2) {
        // Parent is root.
        return 0;
    }

    size_t v = index % PAGE_SIZE;
    if (2 <= v) {
        // Fast path. Parent is on the same page as the child.
        return index - v + (v / 2);
    }

    // Slow path. Parent is on another page.
    v = index / PAGE_SIZE - 1;
    const size_t page_leaves = PAGE_CHUNKS + 1;
    index = v / page_leaves + 1;
    return index * PAGE_SIZE + v % page_leaves - page_leaves + 1;
}

template <typename T, typename Compare>
size_t BHeap<T, Compare>::getChildIndex(size_t u)
{
    assert(u < SIZE_MAX);

    if (u == 0) {
        // Root's child is always 1.
        return 1;
    }

    --u;
    size_t v = u % PAGE_SIZE + 1;
    if (v < PAGE_SIZE / 2) {
        // Fast path. Child is on the same page as the parent.
        if (u > SIZE_MAX - 2 - v) {
            // Child overflow.
            return SIZE_MAX;
        }
        return u + v + 2;
    }

    // Slow path. Child is on another page.
    const size_t page_leaves = PAGE_CHUNKS + 1;
    v += (u / PAGE_SIZE + 1) * page_leaves - PAGE_SIZE;
    if (v > (SIZE_MAX - 1) / PAGE_SIZE) {
        // Child overflow.
        return SIZE_MAX;
    }
    return v * PAGE_SIZE + 1;
}

template <typename T, typename Compare>
void BHeap<T, Compare>::siftUp(size_t rootIndex, size_t holeIndex, const T& item)
{
    assert(holeIndex >= rootIndex);

    while (holeIndex > rootIndex) {
        const size_t parentIndex = getParentIndex(holeIndex);
        assert(parentIndex >= rootIndex);
        const auto& parent = d_c[parentIndex];
        if (!d_comp(parent, item)) {
            break;
        }
        d_c[holeIndex] = std::move(parent);
        holeIndex = parentIndex;
    }
    d_c[holeIndex] = std::move(item);
}

template <typename T, typename Compare>
size_t BHeap<T, Compare>::moveUpMaxChild(
       size_t childrenCount,
       size_t holeIndex,
       size_t childIndex)
{
    assert(childrenCount > 0);
    assert(childrenCount <= 2);
    assert(childIndex == getChildIndex(holeIndex));

    size_t maxChildIndex = childIndex;
    for (size_t i = 1; i != childrenCount; ++i) {
        if (!d_comp(d_c[childIndex + i], d_c[maxChildIndex])) {
            maxChildIndex = childIndex + i;
        }
    }
    d_c[holeIndex] = std::move(d_c[maxChildIndex]);
    return maxChildIndex;
}

template <typename T, typename Compare>
void BHeap<T, Compare>::siftDown(size_t holeIndex, const T& item)
{
    assert(!empty());
    const size_t heapSize = d_c.size();
    assert(holeIndex < heapSize);

    const size_t rootIndex = holeIndex;
    const size_t lastFullIndex = heapSize - (heapSize - 1) % 2;
    while (true) {
        const size_t childIndex = getChildIndex(holeIndex);
        if (childIndex >= lastFullIndex) {
            if (childIndex < heapSize) {
                assert(childIndex == lastFullIndex);
                holeIndex = moveUpMaxChild(heapSize - childIndex, holeIndex, childIndex);
            }
            break;
        }
        assert(heapSize - childIndex >= 2);
        holeIndex = moveUpMaxChild(2, holeIndex, childIndex);
    }
    siftUp(rootIndex, holeIndex, item);
}

template <typename T, typename Compare>
bool BHeap<T, Compare>::isHeap() const
{
    const size_t heapSize = d_c.size();
    for (size_t i = 1; i != heapSize; ++i) {
        const size_t p = getParentIndex(i);
        if (d_comp(d_c[p], d_c[i])) {
            return false;
        }
    }
    return true;
}

template <typename T, typename Compare>
void BHeap<T, Compare>::makeHeap()
{
    const auto heapSize = d_c.size();

    if (1 < heapSize) {
        // Skip leaf nodes without children. This is easy to do for non-paged
        // heap, i.e. when page chunks = 1, but it is difficult for paged heaps.
        // So leaf nodes in paged heaps are visited anyway.
        size_t i = heapSize - 2;
        do {
            auto item = std::move(d_c[i]);
            siftDown(i, item);
        } while (0 < i--);
    }
    assert(isHeap());
}

template <typename T, typename Compare>
void BHeap<T, Compare>::pushHeap()
{
    const size_t heapSize = d_c.size();
    if (1 < heapSize) {
        const size_t u = heapSize - 1;
        auto item = std::move(d_c[u]);
        siftUp(0, u, item);
    }

    assert(isHeap());
}

template <typename T, typename Compare>
BHeap<T, Compare>::BHeap(const Compare& comparer)
    : d_comp(comparer), d_c()
{
}

template <typename T, typename Compare>
template <typename InputIterator>
BHeap<T, Compare>::BHeap(const InputIterator& first,
                         const InputIterator& last,
                         const Compare&       comparer)
    : d_comp(comparer), d_c()
{
    d_c.insert(d_c.end(), first, last);
    makeHeap();
}

template <typename T, typename Compare>
bool BHeap<T, Compare>::empty() const
{
    return d_c.empty();
}

template <typename T, typename Compare>
size_t BHeap<T, Compare>::size() const
{
    return d_c.size();
}

template <typename T, typename Compare>
size_t BHeap<T, Compare>::capacity() const
{
    return d_c.capacity();
}

template <typename T, typename Compare>
const T& BHeap<T, Compare>::top() const
{
    assert(!empty());

    return d_c.front();
}

template <typename T, typename Compare>
void BHeap<T, Compare>::reserve(size_t capacity)
{
    return d_c.reserve(capacity);
}

template <typename T, typename Compare>
void BHeap<T, Compare>::push(const T& v)
{
    d_c.push_back(v);
    pushHeap();
}

template <typename T, typename Compare>
void BHeap<T, Compare>::push(T&& v) {
    d_c.push_back(std::move(v));
    pushHeap();
}

template <typename T, typename Compare>
void BHeap<T, Compare>::pop()
{
    assert(!empty());

    const size_t heapSizeMinusOne = d_c.size() - 1;
    if (heapSizeMinusOne != 0) {
        auto tmp = std::move(d_c[heapSizeMinusOne]);
        d_c[heapSizeMinusOne] = std::move(d_c[0]);
        d_c.pop_back();
        siftDown(0, tmp);
    }
    else {
        d_c.pop_back();
    }

    // TODO: Add back in?: assert(isHeap());
}

template <typename T, typename Compare>
void BHeap<T, Compare>::swap(BHeap& bh)
{
    std::swap(d_comp, bh.d_comp);
    std::swap(d_c, bh.d_c);
}

namespace std {

template <typename T, typename Compare>
void swap(BHeap<T, Compare>& a,
          BHeap<T, Compare>& b) {
    a.swap(b);
}

}

#endif
