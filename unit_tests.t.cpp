#include <bheap.h>

#include <gtest/gtest.h>

#include <cstdlib>
#include <functional>

TEST(bheap, default_constructor_empty)
{
    BHeap<int> empty;
    EXPECT_TRUE(empty.empty());
    EXPECT_EQ(0, empty.size());
}

TEST(bheap, iterator_constructor_not_empty)
{
    std::vector<int> v{1};
    BHeap<int> notEmpty(v.begin(), v.end());
    EXPECT_FALSE(notEmpty.empty());
    EXPECT_EQ(v.size(), notEmpty.size());
}

TEST(bheap, iterator_constructor_of_6)
{
    std::vector<int> v{1, 2, 3, 4, 5, 6};
    BHeap<int> pq(v.begin(), v.end());
    EXPECT_EQ(v.size(), pq.size());
}

TEST(bheap, iterator_constructor_of_1000)
{
    std::vector<int> v;
    for (size_t i = 0; i != 1000; ++i) {
        v.push_back(i);
    }
    BHeap<int> pq(v.begin(), v.end());
    EXPECT_EQ(v.size(), pq.size());
}

TEST(bheap, capacity_empty)
{
    BHeap<int> pq;
    EXPECT_EQ(0, pq.capacity());
}

TEST(bheap, capacity_initilizer)
{
    std::vector<int> v{1, 2, 3, 4, 5, 6};
    BHeap<int> pq(v.begin(), v.end());
    EXPECT_EQ(6, pq.capacity());
}

TEST(bheap, capacity_push)
{
    BHeap<int> pq;
    for (size_t i = 0; i != 6; ++i) {
        pq.push(i);
    }
    EXPECT_EQ(8, pq.capacity());
}

TEST(bheap, top)
{
    std::vector<int> v{1, 2, 3, 4, 5, 6};
    BHeap<int> pq(v.begin(), v.end());
    EXPECT_EQ(6, pq.top());
}

TEST(bheap, top_greater_comp)
{
    std::vector<int> v{1, 2, 3, 4, 5, 6};
    BHeap<int, std::greater<int>> pq(v.begin(), v.end(), std::greater<int>());
    EXPECT_EQ(1, pq.top());
}

TEST(bheap, reserve)
{
    BHeap<int> pq;
    pq.reserve(1000);
    EXPECT_EQ(1000, pq.capacity());
}

TEST(bheap, push)
{
    BHeap<int> pq;
    const size_t n = 6;
    for (size_t i = 0; i != n; ++i) {
        pq.push(rand());
        EXPECT_EQ(i + 1, pq.size());
    }
}

TEST(bheap, pop)
{
    std::vector<int> v{1, 2, 3, 4, 5, 6};
    BHeap<int> pq(v.begin(), v.end());
    int max = pq.top();
    const size_t n = pq.size();
    for (size_t i = 1; i != n; ++i) {
        pq.pop();
        EXPECT_EQ(n - i, pq.size());
        EXPECT_GE(max, pq.top());
        max = pq.top();
    }
    EXPECT_GE(max, pq.top());
    pq.pop();
    EXPECT_TRUE(pq.empty());
}

TEST(bheap, interleave_push_and_pop)
{
    BHeap<int> pq;
    const size_t n = 100;
    for (size_t i = 0; i != n; ++i) {
        pq.push(rand());
    }

    int max = pq.top();
    for (size_t i = 0; i != n; ++i) {
        pq.pop();
        EXPECT_GE(max, pq.top());
        const int tmp = rand();
        if (max < tmp) {
            max = tmp;
        }
        pq.push(tmp);
    }
}

TEST(bheap, swap)
{
    std::vector<int> v{1, 2, 3, 4, 5, 6};
    BHeap<int> notEmpty(v.begin(), v.end());
    BHeap<int> empty;
    notEmpty.swap(empty);
    EXPECT_TRUE(notEmpty.empty());
    EXPECT_FALSE(empty.empty());
    EXPECT_EQ(6, empty.size());
    std::swap(notEmpty, empty);
    EXPECT_FALSE(notEmpty.empty());
    EXPECT_EQ(6, notEmpty.size());
    EXPECT_TRUE(empty.empty());
}
