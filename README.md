# BHeap

A BHeap is a variant of the heap data structure that takes advantage of the
cache layout in virtual memory. BHeap follows the same protocol from
`priority_queue`.

## Benchmarks

TODO: Current benchmarks across the board show the implementation is inefficient.
