CC    = g++
FLAGS = -std=c++14 -Wall -Wextra -Werror -pedantic -I.

HEADER_FLAGS = $(FLAGS) -fsyntax-only
DEBUG_FLAGS  = $(FLAGS) -g --coverage
OPT_FLAGS    = $(FLAGS) -DNDEBUG -O3
GTEST_FLAGS  = -L/usr/lib/ -lgtest_main -lgtest -pthread

.PHONY: all
all: coverage bench

.PHONY: header
header:
	@echo "Verifing Header"
	@$(CC) $(HEADER_FLAGS) bheap.h

.PHONY: test
test: header
	@echo "Testing"
	@$(CC) $(DEBUG_FLAGS) $(GTEST_FLAGS) unit_tests.t.cpp -o unit_tests
	@valgrind ./unit_tests

.PHONY: coverage
coverage: test
	@echo "Coverage"
	@gcov -abcdfimnu unit_tests.t.cpp | grep -A 4 "bheap.h"

.PHONY: cov
cov: coverage

.PHONY:bench
bench: header
	@echo "Benchmark"
	@$(CC) $(OPT_FLAGS) bench.t.cpp -o bench
	@./bench

.PHONY: clean
clean:
	@echo "Cleaning"
	@rm -f unit_tests bench *.gcda *.gcno
